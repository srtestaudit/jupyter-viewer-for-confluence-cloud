## Code Block without Language

```
function greet(name) {
  return "Hello, " + name + "!";
}

console.log(greet("Alice"));
```