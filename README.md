# README #
Jira issue: BTP-245

## Image via HTML 

<img src="defender_rocket.png" alt="Alt text" width="50"/>

Jira issue: https://srtestaudit.atlassian.net/browse/BTP-245

[Relative Link](defender_rocket.png)

[Absolute Link](https://bitbucket.org/srtestaudit/jupyter-viewer-for-confluence-cloud/src/master/defender_rocket.png)

[Nested Relative Link](folder1/globe_poor.png)

[Nested MD Relative Link](folder1/README.md)

[Nested Directory with Spaces Relative Link](directory with spaces/README.md)

[Nested Directory without Spaces Relative Link](directoryWithoutSpaces/README.md)

![Image](defender_rocket.png)

![Test](svgTextExample.svg)

IMAGE without a paragraph 

<img src="defender_rocket.png" alt="Alt text" width="50"/>


Hola Amigos


IMAGE HTML TAG 
<img src="defender_rocket.png" alt="Alt text" width="50"/>
<img src="defender_rocket.png" alt="Alt text" />

IMAGE HTML TAG
<img src="defender_rocket.png" width="100" alt="Alt text"/>

IMAGE HTML TAG
<img src="defender_rocket.png" alt="Alt text"/>

Emoji 
😀


It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

</details> 

<details><summary>Change History Info</summary> 

> A history of significant changes or additions to the product. The change reference would typically be a portfolio reference number, or a Service Now ticket number, and the Github tag associated with the change. 

| Date | Description | Change reference |
| ---------- | ---------------- | ------------------------ |
| 29-Sep-22 | Initial Version    | Portfolio 0860                  |


</details> 

Dimensions | Megapixels
---|---
1,920 x 1,080 | 2.1MP
3,264 x 2,448 | 8MP
4,288 x 3,216 | 14MP


<table border="1">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>John</td>
            <td>Doe</td>
            <td>johndoe@example.com</td>
        </tr>
        <tr>
            <td>Jane</td>
            <td>Smith</td>
            <td>janesmith@example.com</td>
        </tr>
    </tbody>
</table>

# Welcome to the exercise on enabling GitHub Pages!

This exercise is an exercise to check your knowledge on enabling GitHub Pages. It is automatically graded via a workflow once you have completed the instructions.

**Quick links:**
- [About this exercise](#about-this-exercise)
- [Instructions](#instructions)
- [Seeing your result](#seeing-your-result)
- [Troubleshooting](#troubleshooting)
- [Useful resources](#useful-resources)

## About this exercise

:warning: A grading script and a setup utility exist inside of the `.github/` directory. You do not need to use these workflows for any purpose and **altering their contents will affect the repository's ability to assess your exercise and give feedback.**

:warning: This exercise utilizes [GitHub Actions](https://docs.github.com/en/actions), which is free for public repositories and self-hosted runners, but may incur charges on private repositories. See *[About billing for GitHub Actions]* to learn more.

:information_source: The use of GitHub Actions also means that it may take the grading workflow a few seconds and sometimes minutes to run.

## Instructions

1. Create your own copy of this repository by using the [Use this template](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-from-a-template#creating-a-repository-from-a-template) button. Make sure you set your repository visibility to **Public** when asked.
2. In your newly created repository, enable GitHub Pages with the `main` branch and the `docs/` directory as the source for the GitHub Pages site.

## Seeing your result

Your exercise is graded automatically once you have completed the instructions. To see the result of your exercise, click the **Actions** tab, select the **Grading workflow**, and select the most recent workflow run. The status of the workflow indicates if you have passed or failed the exercise.

If the workflow failed, scroll down to the **Annotations** section to check what went wrong.

See *[Viewing workflow run history]* if you need assistance.

## Troubleshooting

 If the grading workflow does not automatically run after you complete the instructions, run the troubleshooter: in the **Actions** tab select the **Grading workflow**, click **Run workflow**, select the appropriate branch (usually `main`), and click the **Run workflow** button.

 See *[Running a workflow on GitHub]* if you need assistance.

## Useful resources

Use these to help you!

Resources specific to this exercise:
- [Configuring a publishing source for your GitHub Pages site]

Resources for working with exercise and GitHub Actions in general:
- [Creating a repository from a template]
- [Viewing workflow run history]
- [Running a workflow on GitHub]
- [About billing for GitHub Actions]
- [GitHub Actions]

<!--
Links used throughout this README:
-->
[Configuring a publishing source for your GitHub Pages site]:   https://docs.github.com/en/github/working-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site
[Creating a repository from a template]:                        https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-from-a-template
[Viewing workflow run history]:                                 https://docs.github.com/en/actions/managing-workflow-runs/viewing-workflow-run-history
[Running a workflow on GitHub]:                                 https://docs.github.com/en/actions/managing-workflow-runs/manually-running-a-workflow#running-a-workflow-on-github
[About billing for GitHub Actions]:                             https://docs.github.com/en/github/setting-up-and-managing-billing-and-payments-on-github/about-billing-for-github-actions
[GitHub Actions]:                                               https://docs.github.com/en/actions

**CODEOWNERS**

```
CODEOWNERS.destination_branch_pattern main
CODEOWNERS.destination_branch_pattern release/*
CODEOWNERS.toplevel.subdirectory_overrides enable
CODEOWNERS.toplevel.assignment_routing random 2
CODEOWNERS.toplevel.create_pull_request_comment disable
CODEOWNERS.toplevel.auto_unapprove_on_change enable
CODEOWNERS.source_branch_exclusion_pattern hotfix/*

@@@MyDevs                @PeterTheHacker  @PeterTheJavaExpert ann@scala.lang @@JSDevs

*                        @PeterTheHacker
*.java                   @PeterTheJavaExpert
*.js                     @PaulTheJSGuru @@JSExperts
"a/path with spaces/*"   docs@example.com
!ci/playgrounds.yml
src/components/**/*.js   @@MyDevs

Check(@@MyDevs >= 2)
```


**devsensei.yaml**

```yaml
shared:
  - custom-groups:
      MyDevs:
        - @PeterTheHacker
        - @PeterTheJavaExpert
        - ann@scala.lang 
        - @@JSDevs
      
workflows:
  - name: Add Code Owners
    conditions:
      - or:
        - destination=main
        - destination~=release/*
      - source~!=hotfix/*
    actions:
      - add-codeowners:
          auto-unapprove-on-change: true      
          assignment-routing:
            random: 2
          custom-groups:
            MyDevs: *MyDevs 
          rules: |
            *                       @PeterTheHacker
            *.java                  @PeterTheJavaExpert
            *.js                    @PaulTheJSGuru @@JSExperts
            "a/path with spaces/*"  docs@example.com
            !ci/playgrounds.yml
            src/components/**/*.js  @@MyDevs
            Check(@@MyDevs >= 2)
```

```js
console.log("testing")
```

```plantuml
@startuml
Alice -> Bob: Hello
@enduml
```


```groovy
package org.jacoco.examples.maven.groovy

class HelloWorld {

	String getMessage(boolean bigger) {
		if (bigger) {
			return "Hello Universe!";
		} else {
			return "Hello World!";
		}
	}

}
```

# README #

## Markdown Basic Elements ##

### List ###

* some thing
* more thing
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Blockquote ###

> A blockquote would look great here.
 
### Code ###

`code looks good here`

### Image ###

Hey **Image!**
![Rocket](defender_rocket.png)

### Image with Spaces ###
![Image](images/image%20with%20spaces.png)

Note, default markdown doesn't interpret spaces in path. 
So this will be text, depending on the Markdown Implementation:
![Image](images/image with spaces.png)