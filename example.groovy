def name = "Groovy"  // 'def' is used for dynamic typing
String language = "Groovy"  // Explicit type declaration
int age = 10
boolean isFun = true