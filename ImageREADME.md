### Image with Spaces ###
![Image](images/image%20with%20spaces.png)

Note, default markdown doesn't interpret spaces in path.
So this will be text, depending on the Markdown Implementation:
![Image](images/image with spaces.png)
